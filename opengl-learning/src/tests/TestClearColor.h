#pragma once

#include "Test.h"

namespace test
{

class TestClearColor final : public Test
{
public:
	TestClearColor();
	~TestClearColor();

	void OnUpdate(const float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	float m_ClearColor[4];
};

}	 // namespace test
