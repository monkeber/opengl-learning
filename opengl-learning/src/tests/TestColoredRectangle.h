#pragma once

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "Test.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class IndexBuffer;
class Renderer;
class Shader;
class VertexArray;
class VertexBuffer;
class VertexBufferLayout;

namespace test
{

class TestColoredRectangle final : public Test
{
public:
	TestColoredRectangle();
	~TestColoredRectangle();

	void OnUpdate(const float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	std::unique_ptr<IndexBuffer> m_IndexBuffer;
	std::unique_ptr<VertexArray> m_VertexArray;
	std::unique_ptr<VertexBuffer> m_VertexBuffer;
	std::unique_ptr<VertexBufferLayout> m_VertexBufferLayout;
	std::unique_ptr<Shader> m_Shader;
	std::unique_ptr<Renderer> m_Renderer;
	glm::mat4 m_Projection;
	glm::mat4 m_View;
	std::vector<std::tuple<glm::vec3, std::array<float, 4>>> m_Rectangles;
};

}	 // namespace test
