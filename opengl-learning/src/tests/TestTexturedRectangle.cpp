#include "tests/TestTexturedRectangle.h"

#include "imgui/imgui.h"

#include "IndexBuffer.h"
#include "Renderer.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

namespace test
{

TestTexturedRectangle::TestTexturedRectangle()
	: m_Projection{ glm::ortho(0.0f, 960.0f, 0.0f, 540.f, -1.0f, 1.0f) }
	, m_View{ glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)) }
{
	float positions[] = {
		-50.0f, -50.0f, 0.0f, 0.0f,	   // 0
		50.0f,	-50.0f, 1.0f, 0.0f,	   // 1
		50.0f,	50.0f,	1.0f, 1.0f,	   // 2
		-50.0f, 50.0f,	0.0f, 1.0f	   // 3
	};

	unsigned int indices[] = {
		0, 1, 2, 2, 3, 0,
	};

	m_VertexArray = std::make_unique<VertexArray>();
	m_VertexBuffer = std::make_unique<VertexBuffer>(
		positions, 4u * 4u * static_cast<unsigned int>(sizeof(float)));
	m_VertexBufferLayout = std::make_unique<VertexBufferLayout>();
	m_VertexBufferLayout->Push<float>(2);
	m_VertexBufferLayout->Push<float>(2);
	m_VertexArray->AddBuffer(*m_VertexBuffer, *m_VertexBufferLayout);

	m_IndexBuffer = std::make_unique<IndexBuffer>(indices, 6);

	m_Shader = std::make_unique<Shader>("res/shaders/Basic.shader");
	m_Shader->Bind();

	m_Shader->SetUniform1i("u_UseTexture", 1);

	m_Rectangles.emplace_back(std::make_tuple(
		glm::vec3{ 200, 200, 0 }, std::make_unique<Texture>("res/textures/deus-ex.png")));

	m_Rectangles.emplace_back(std::make_tuple(
		glm::vec3{ 400, 200, 0 }, std::make_unique<Texture>("res/textures/adam-jensen-face.png")));

	m_Renderer = std::make_unique<Renderer>();
}

TestTexturedRectangle::~TestTexturedRectangle()
{
	m_VertexArray->Unbind();
	m_VertexBuffer->Unbind();
	m_IndexBuffer->Unbind();
	m_Shader->Unbind();
}

void TestTexturedRectangle::OnUpdate(const float deltaTime)
{
}

void TestTexturedRectangle::OnRender()
{
	m_Renderer->Clear();
	m_Shader->Bind();

	for (int i = 0; i < m_Rectangles.size(); ++i)
	{
		std::get<1>(m_Rectangles[i])->Bind(i);
		m_Shader->SetUniform1i("u_Texture", i);

		glm::mat4 model = glm::translate(glm::mat4(1.0f), std::get<0>(m_Rectangles[i]));
		glm::mat4 mvp = m_Projection * m_View * model;
		m_Shader->SetUniformMat4f("u_MVP", mvp);

		m_Renderer->Draw(*m_VertexArray, *m_IndexBuffer, *m_Shader);
	}
}

void TestTexturedRectangle::OnImGuiRender()
{
	for (size_t i = 0; i < m_Rectangles.size(); ++i)
	{
		const std::string translationName = "Translation " + std::to_string(i);
		ImGui::SliderFloat3(translationName.c_str(), &std::get<0>(m_Rectangles[i]).x, 0.0f, 960.0f);
	}
}

}	 // namespace test
