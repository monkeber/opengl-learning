#pragma once

namespace test
{

class Test
{
public:
	Test()
	{
	}
	virtual ~Test()
	{
	}

	virtual void OnUpdate(const float deltaTime)
	{
	}
	virtual void OnRender()
	{
	}
	virtual void OnImGuiRender()
	{
	}
};

}	 // namespace test
