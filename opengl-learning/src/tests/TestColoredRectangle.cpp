#include "tests/TestColoredRectangle.h"

#include "imgui/imgui.h"

#include "IndexBuffer.h"
#include "Renderer.h"
#include "Shader.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

namespace test
{

TestColoredRectangle::TestColoredRectangle()
	: m_Projection{ glm::ortho(0.0f, 960.0f, 0.0f, 540.f, -1.0f, 1.0f) }
	, m_View{ glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)) }
{
	float positions[] = {
		-50.0f, -50.0f, 0.0f, 0.0f,	   // 0
		50.0f,	-50.0f, 1.0f, 0.0f,	   // 1
		50.0f,	50.0f,	1.0f, 1.0f,	   // 2
		-50.0f, 50.0f,	0.0f, 1.0f	   // 3
	};

	unsigned int indices[] = {
		0, 1, 2, 2, 3, 0,
	};

	m_VertexArray = std::make_unique<VertexArray>();
	m_VertexBuffer = std::make_unique<VertexBuffer>(
		positions, 4u * 4u * static_cast<unsigned int>(sizeof(float)));
	m_VertexBufferLayout = std::make_unique<VertexBufferLayout>();
	m_VertexBufferLayout->Push<float>(2);
	m_VertexBufferLayout->Push<float>(2);
	m_VertexArray->AddBuffer(*m_VertexBuffer, *m_VertexBufferLayout);

	m_IndexBuffer = std::make_unique<IndexBuffer>(indices, 6);

	m_Shader = std::make_unique<Shader>("res/shaders/Basic.shader");

	m_Renderer = std::make_unique<Renderer>();

	m_Rectangles.push_back({ { 200, 200, 0 }, { 0.2f, 0.3f, 0.8f, 1.0f } });
	m_Rectangles.push_back({ { 400, 200, 0 }, { 0.2f, 0.3f, 0.8f, 1.0f } });
}

TestColoredRectangle::~TestColoredRectangle()
{
	m_VertexArray->Unbind();
	m_VertexBuffer->Unbind();
	m_IndexBuffer->Unbind();
	m_Shader->Unbind();
}

void TestColoredRectangle::OnUpdate(const float deltaTime)
{
}

void TestColoredRectangle::OnRender()
{
	m_Renderer->Clear();
	m_Shader->Bind();

	for (auto& rectangle : m_Rectangles)
	{
		m_Shader->SetUniform4f(
			"u_Color",
			std::get<1>(rectangle)[0],
			std::get<1>(rectangle)[1],
			std::get<1>(rectangle)[2],
			std::get<1>(rectangle)[3]);
		glm::mat4 model = glm::translate(glm::mat4(1.0f), std::get<0>(rectangle));
		glm::mat4 mvp = m_Projection * m_View * model;
		m_Shader->Bind();
		m_Shader->SetUniformMat4f("u_MVP", mvp);
		m_Renderer->Draw(*m_VertexArray, *m_IndexBuffer, *m_Shader);
	}
}

void TestColoredRectangle::OnImGuiRender()
{
	for (size_t i = 0; i < m_Rectangles.size(); ++i)
	{
		const std::string colorName = "Rectangle Color " + std::to_string(i);
		ImGui::ColorEdit4(colorName.c_str(), std::get<1>(m_Rectangles[i]).data());
		const std::string translationName = "Translation " + std::to_string(i);
		ImGui::SliderFloat3(translationName.c_str(), &std::get<0>(m_Rectangles[i]).x, 0.0f, 960.0f);
	}
}

}	 // namespace test
